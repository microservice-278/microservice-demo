package com.datlm.producerservice.controller;

import com.datlm.producerservice.dto.request.ProducerRequest;
import com.datlm.producerservice.entity.Producer;
import com.datlm.producerservice.service.ProducerService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/rest/producer")
@RequiredArgsConstructor
public class ProducerController {

    private final Logger L = LoggerFactory.getLogger(ProducerController.class);

    private final ProducerService producerService;

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createProducer(@RequestBody ProducerRequest request) {
        L.info("[POST] {}: create a new producer", "/api/rest/producer/create");
        producerService.createProducer(request);
        return ResponseEntity.ok().body("\"create producer successfully!\"");
    }

    @GetMapping(value = "/find-all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Producer>> findAllProducer() {
        L.info("[GET] {}: find all producer", "/api/rest/producer/find-all");
        return ResponseEntity.ok().body(producerService.findAll());
    }

}
