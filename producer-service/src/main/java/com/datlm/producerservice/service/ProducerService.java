package com.datlm.producerservice.service;

import com.datlm.producerservice.dto.request.ProducerRequest;
import com.datlm.producerservice.entity.Producer;

import java.util.List;

public interface ProducerService {

    void createProducer(ProducerRequest producerRequest);

    List<Producer> findAll();

}
