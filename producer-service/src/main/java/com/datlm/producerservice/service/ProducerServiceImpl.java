package com.datlm.producerservice.service;

import com.datlm.producerservice.dto.request.ProducerRequest;
import com.datlm.producerservice.entity.Producer;
import com.datlm.producerservice.repository.ProducerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProducerServiceImpl implements ProducerService {

    private final ProducerRepository producerRepository;

    @Override
    public void createProducer(ProducerRequest producerRequest) {
        Producer newProducer = Producer.builder()
                .name(producerRequest.getName())
                .description(producerRequest.getDescription())
                .build();

        producerRepository.save(newProducer);
    }

    @Override
    public List<Producer> findAll() {
        return producerRepository.findAll();
    }
}
