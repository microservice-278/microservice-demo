package com.datlm.producerservice.repository;

import com.datlm.producerservice.entity.Producer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProducerRepository extends JpaRepository<Producer, Long> {
}
