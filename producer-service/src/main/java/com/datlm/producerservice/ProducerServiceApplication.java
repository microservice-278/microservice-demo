package com.datlm.producerservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProducerServiceApplication {

	private static final Logger L = LoggerFactory.getLogger(ProducerServiceApplication.class);
	public static void main(String[] args) {

		System.setProperty("spring.devtools.restart.enabled", "true");
		System.setProperty("java.net.preferIPv4Stack", "true");

		SpringApplication.run(ProducerServiceApplication.class, args);

		L.info("-----------------------------------------------------------");
		L.info("            Welcome to Producer Service.");
		L.info("         Producer Service start successfully.");
		L.info("-----------------------------------------------------------");
	}

}
