package com.datlm.producerservice.dto.request;

import lombok.Data;

@Data
public class ProducerRequest {

    private String name;
    private String description;

}
