package com.datlm.productservice.service;

import com.datlm.productservice.dto.request.ProductRequest;
import com.datlm.productservice.entity.Product;

import java.util.List;

public interface ProductService {

    void createProduct(ProductRequest productRequest);

    List<Product> findAllProduct();

    List<Product> findProductByProducer(Long producerId);

}
