package com.datlm.productservice.service;

import com.datlm.productservice.dto.request.ProductRequest;
import com.datlm.productservice.entity.Product;
import com.datlm.productservice.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public void createProduct(ProductRequest productRequest) {

        Product newProduct = Product.builder()
                .name(productRequest.getName())
                .price(productRequest.getPrice())
                .producerId(productRequest.getProducerId())
                .build();

        productRepository.save(newProduct);
    }

    @Override
    public List<Product> findAllProduct() {
        return productRepository.findAll();
    }

    @Override
    public List<Product> findProductByProducer(Long producerId) {
        return productRepository.findAllByProducerId(producerId);
    }
}
