package com.datlm.productservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductServiceApplication {

    private static final Logger L = LoggerFactory.getLogger(ProductServiceApplication.class);

    public static void main(String[] args) {
        System.setProperty("spring.devtools.restart.enabled", "true");
        System.setProperty("java.net.preferIPv4Stack", "true");

        SpringApplication.run(ProductServiceApplication.class, args);

        L.info("-----------------------------------------------------------");
        L.info("            Welcome to Product Service.");
        L.info("         Product Service start successfully.");
        L.info("-----------------------------------------------------------");
    }


}
