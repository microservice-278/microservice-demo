package com.datlm.productservice.controller;

import com.datlm.productservice.dto.request.ProductRequest;
import com.datlm.productservice.entity.Product;
import com.datlm.productservice.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/rest/product")
@RequiredArgsConstructor
public class ProductController {

    private final Logger L = LoggerFactory.getLogger(ProductController.class);

    private final ProductService productService;

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createProduct(@RequestBody ProductRequest productRequest) {
        L.info("[POST] {}: create a new product", "/api/rest/product/create");
        productService.createProduct(productRequest);
        return ResponseEntity.ok().body("\"create product successfully!\"");
    }

    @GetMapping(value = "/find-all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Product>> findAllProduct() {
        L.info("[GET] {}: find all products", "/api/rest/product/find-all");
        return ResponseEntity.ok().body(productService.findAllProduct());
    }

    @GetMapping(value = "/find-by-producer", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Product>> findProductByProducer(@RequestParam(name = "producerId") Long id) {
        L.info("[GET] {}: find all products by producer", "/api/rest/product/find-by-producer?producerId=" + id);
        return ResponseEntity.ok().body(productService.findProductByProducer(id));
    }

}
